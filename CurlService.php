<?php

namespace CYINT\ComponentsPHP\Services;
class CurlService
{
    protected $curl;

    public function __construct()
    {
		$this->curl = curl_init();
		$this->configureCurl();
    }

    public function configureCurl($config = null)
	{
		if(empty($config))
		{
			$config = [
				CURLOPT_RETURNTRANSFER => true,
				CURLOPT_SSL_VERIFYPEER => false,
				CURLOPT_SSL_VERIFYHOST => 2
			];
		}

		curl_setopt_array($this->curl, $config);
	}


    public function reinitialize()
    {
        curl_close($this->curl);
        $this->curl = curl_init();
        $this->configureCurl();
    }

	public function processUrl($url)
	{

		 curl_setopt($this->curl, CURLOPT_URL, $url);
		 $result = curl_exec($this->curl);
         $this->reinitialize();
		 return $result;
	}

    public function setPostData($data)
    {
        curl_setopt($this->curl, CURLOPT_POST, true);
        curl_setopt($this->curl, CURLOPT_POSTFIELDS, $data);
    }
}

?>
